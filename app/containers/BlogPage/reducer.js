/*
 *
 * BlogPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOAD_POSTS,
  LOAD_POSTS_SUCCESS,
  LOAD_POSTS_FAILURE,
} from './constants';

const initialState = fromJS({
  posts: { ids: [] },
  loading: false,
  error: false,
});

function blogPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_POSTS:
      return state
        .set('loading', true);
    case LOAD_POSTS_SUCCESS:
      return state
        .set('posts', action.posts)
        .set('loading', false)
        .set('error', false);
    case LOAD_POSTS_FAILURE:
      return state
        .set('loading', false)
        .set('error', true);
    default:
      return state;
  }
}

export default blogPageReducer;
