/*
 * BlogPage Messages
 *
 * This contains all the text for the BlogPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'blog.containers.BlogPage.header',
    defaultMessage: 'Human and Programming languages intersection',
  },
});
