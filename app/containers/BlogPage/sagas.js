import { take, call, put, fork, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_POSTS } from './constants';
import { postsLoaded, postsLoadingError } from './actions';
import { getRequestBodyResponse, API_HOST } from 'utils/request';

/**
 * BlogApi posts request/response handler
 */
export function* listPosts(locale) {
  const requestURL = `${API_HOST}/v1/posts?locale=${locale}`;

  try {
    // Call our request helper (see 'utils/request')
    const postsResponse = yield call(getRequestBodyResponse, requestURL);
    const posts = postsResponse.data.reduce((reducedPosts, postItem) => {
      const post = {
        id: postItem.id,
        title: postItem.attributes.title,
        content: postItem.attributes.content,
        permalink: postItem.attributes.permalink,
        publishedDate: postItem.attributes['published-time'],
      };
      const newPosts = reducedPosts;
      newPosts[postItem.id] = post;
      newPosts.ids.push(postItem.id);
      return newPosts;
    }, { ids: [] });
    yield put(postsLoaded(posts));
  } catch (err) {
    yield put(postsLoadingError(err));
  }
}

/**
 * Watches for LOAD_POSTS actions and calls getPosts when one comes in.
 */
export function* getPostsWatcher() {
  const { locale } = yield take(LOAD_POSTS);
  yield fork(listPosts, locale);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* postsData() {
  // Fork watcher so we can continue execution
  const watcher = yield fork(getPostsWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// Bootstrap sagas
export default [
  postsData,
];
