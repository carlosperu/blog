import { createSelector } from 'reselect';

/**
 * Direct selector to the postNew state
 */
const selectBlog = () => (state) => state.get('blogPage');

/**
 * Default selector used by BlogPage
 */

const selectPosts = () => createSelector(
  selectBlog(),
  (blogState) => blogState.get('posts'),
);

const selectLoading = () => createSelector(
  selectBlog(),
  (blogState) => blogState.get('loading'),
);

const selectError = () => createSelector(
  selectBlog(),
  (blogState) => blogState.get('error'),
);

export {
  selectBlog,
  selectPosts,
  selectLoading,
  selectError,
};
