/*
 *
 * BlogPage constants
 *
 */

export const LOAD_POSTS = 'blog/BlogPage/LOAD_POSTS';
export const LOAD_POSTS_SUCCESS = 'blog/BlogPage/LOAD_POSTS_SUCCESS';
export const LOAD_POSTS_FAILURE = 'blog/BlogPage/LOAD_POSTS_FAILURE';
