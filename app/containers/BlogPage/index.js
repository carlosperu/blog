/*
 *
 * BlogPage
 *
 */

import React, { PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import PostList from 'components/PostList';
import { createStructuredSelector } from 'reselect';
import LoadingIndicator from 'components/LoadingIndicator';
import { loadPosts } from './actions';
import List from 'components/List';
import ListItem from 'components/ListItem';
import { selectPosts, selectLoading, selectError } from './selectors';
import { selectLocale } from '../LanguageProvider/selectors';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

export class BlogPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    this.props.loadPosts(this.props.locale);
  }

  render() {
    let mainContent = null;

    // Show a loading indicator when we're loading
    if (this.props.loading) {
      mainContent = (<LoadingIndicator />);

    // Show an error if there is one
    } else if (this.props.error !== false) {
      const ErrorComponent = () => (
        <ListItem item={'Something went wrong, please try again!'} />
      );
      mainContent = (<List component={ErrorComponent} />);

    // If we're not loading, don't have an error and there are posts, show the posts
    } else if (this.props.posts !== false) {
      mainContent = (<PostList posts={this.props.posts} />);
    }
    return (
      <section>
        <Helmet
          title="Blog"
          meta={[
            { name: 'description', content: 'This blog is about all topics that interest me' },
          ]}
        />
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <h2><FormattedMessage {...messages.header} /></h2>
              <hr className="star-primary" />
            </div>
          </div>
          {mainContent}
        </div>
      </section>
    );
  }
}

BlogPage.propTypes = {
  posts: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  locale: PropTypes.string.isRequired,
  loadPosts: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  posts: selectPosts(),
  loading: selectLoading(),
  error: selectError(),
  locale: selectLocale(),
});

export function mapDispatchToProps(dispatch) {
  return {
    loadPosts: (locale) => dispatch(loadPosts(locale)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BlogPage);
