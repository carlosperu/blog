/*
 *
 * BlogPage actions
 *
 */

import {
  LOAD_POSTS,
  LOAD_POSTS_SUCCESS,
  LOAD_POSTS_FAILURE,
} from './constants';

/**
 * Load the posts, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_POSTS
 */
export function loadPosts(locale) {
  return {
    type: LOAD_POSTS,
    locale,
  };
}

/**
 * Dispatched when the posts are loaded by the request saga
 *
 * @param  {array} posts The posts data
 *
 * @return {object}      An action object with a type of LOAD_POSTS_SUCCESS passing the posts
 */
export function postsLoaded(posts) {
  return {
    type: LOAD_POSTS_SUCCESS,
    posts,
  };
}

/**
 * Dispatched when loading the posts fails
 *
 * @param  {boolean} error The error
 *
 * @return {object}       An action object with a type of LOAD_POSTS_FAILURE passing the error
 */
export function postsLoadingError(error) {
  return {
    type: LOAD_POSTS_FAILURE,
    error,
  };
}
