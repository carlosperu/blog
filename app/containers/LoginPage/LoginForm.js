import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form/immutable';
import messages from './messages';
import { FormattedMessage } from 'react-intl';

const RenderField = ({ input, type, meta: { touched, error } }) => (
  <div>
    <input {...input} className="form-control" type={type} />
    {touched && error && <span>{error}</span>}
  </div>
);

const LoginForm = (props) => {
  const { error, handleSubmit, submitting } = props;
  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="email"><FormattedMessage {...messages.email} /></label>
        <Field name="email" type="email" component={RenderField} />
      </div>
      <div className="form-group">
        <label htmlFor="password"><FormattedMessage {...messages.password} /></label>
        <Field name="password" type="password" component={RenderField} />
      </div>
      {error && <strong>{error}</strong>}
      <div>
        <button type="submit" className="btn btn-primary" disabled={submitting}><FormattedMessage {...messages.login} /></button>
      </div>
    </form>
  );
};

RenderField.propTypes = {
  input: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

LoginForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.string,
  submitting: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: 'loginForm',  // a unique identifier for this form
})(LoginForm);
