import { take, call, put, fork, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_LOGIN } from '../App/constants';
import { sessionBegun, errorSigningIn } from '../App/actions';
import { postRequest, API_HOST } from 'utils/request';
import { SubmissionError } from 'redux-form';

/**
 * Github posts request/response handler
 */
export function* startSession(email, password, resolve, reject) {
  const requestURL = `${API_HOST}/auth/sign_in`;

  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(postRequest, requestURL, {
      email,
      password,
    }, {
      headers: { 'Content-Type': 'application/json' },
    });
    if (response.name && response.name === 'Error') {
      throw new SubmissionError({ _error: 'Username or password is invalid.' });
    } else {
      const session = JSON.stringify({
        'access-token': response.headers['access-token'],
        'token-type': response.headers['token-type'],
        client: response.headers.client,
        expiry: response.headers.expiry,
        uid: response.headers.uid,
      });
      yield put(sessionBegun(session));
      yield call(resolve);
    }
  } catch (error) {
    yield call(reject, error);
    yield put(errorSigningIn(error));
  }
}

/**
 * Watches for LOAD_LOGIN actions and calls getPosts when one comes in.
 * By using `takeLatest` only the result of the latest API call is applied.
 */
export function* getSessionWatcher() {
  while (true) {
    const { email, password, resolve, reject } = yield take(LOAD_LOGIN);
    yield fork(startSession, email, password, resolve, reject);
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export function* sessionData() {
  // Fork watcher so we can continue execution
  const watcher = yield fork(getSessionWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// Bootstrap sagas
export default [
  sessionData,
];
