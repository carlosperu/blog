/*
 *
 * LoginPage
 *
 */
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Helmet from 'react-helmet';
import LoginForm from './LoginForm';
import { selectLocale } from '../LanguageProvider/selectors';
import { login } from '../App/actions';

export class LoginPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
  }


  /**
   * Handles login by invoking the login prop, which will dispatc the login
   * action.
   */
  handleLogin(values) {
    return this.props.login(values.get('email'), values.get('password'));
  }

  render() {
    return (
      <section>
        <div className="container">
          <Helmet
            title="FormPage"
            meta={[
              { name: 'description', content: 'Description of FormPage' },
            ]}
          />
          <div className="row">
            <div className="col-md-4 col-md-offset-4">
              {/*
                handleLogin will handle onSubmit and the locale prop is used
                to handle the locale inside the LoginForm component.
              */}
              <LoginForm onSubmit={this.handleLogin} locale={this.props.locale} />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
  locale: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  locale: selectLocale(),
});

export function mapDispatchToProps(dispatch) {
  return {
    login: (email, password) => new Promise((resolve, reject) => {
      dispatch(login(email, password, resolve, reject));
    }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
