import { createSelector } from 'reselect';

/**
 * The global state selectors
 */
const selectPostPage = () => (state) => state.get('postPage');

/**
 * Default selector used by BlogPage
 */

const selectPost = () => createSelector(
  selectPostPage(),
  (postState) => postState.get('post'),
);

const selectTags = () => createSelector(
 selectPostPage(),
 (postState) => postState.get('tags'),
);

const selectLoading = () => createSelector(
  selectPostPage(),
  (postState) => postState.get('loading'),
);

const selectError = () => createSelector(
  selectPostPage(),
  (postState) => postState.get('error'),
);

export {
  selectPostPage,
  selectPost,
  selectTags,
  selectLoading,
  selectError,
};
