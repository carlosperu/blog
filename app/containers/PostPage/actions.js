/*
 *
 * PostPage actions
 *
 */

 import {
   LOAD_POST,
   LOAD_POST_SUCCESS,
   LOAD_POST_FAILURE,
 } from './constants';

 /**
  * Load the post, this action starts the request saga
  *
  * @return {object} An action object with a type of LOAD_POST
  */
 export function loadPost(permalink, session) {
   return {
     type: LOAD_POST,
     permalink,
     session,
   };
 }

 /**
  * Dispatched when the post is loaded by the request saga
  *
  * @param  {object} post The post data
  *
  * @param  {object} tags The tags data
  *
  * @return {object}      An action object with a type of LOAD_POST_SUCCESS passing the post
  */
 export function postLoaded(post, tags) {
   return {
     type: LOAD_POST_SUCCESS,
     post,
     tags,
   };
 }

 /**
  * Dispatched when loading the post fails
  *
  * @param  {boolean} error The error
  *
  * @return {object}       An action object with a type of LOAD_POST_FAILURE passing the error
  */
 export function postLoadingError(error) {
   return {
     type: LOAD_POST_FAILURE,
     error,
   };
 }
