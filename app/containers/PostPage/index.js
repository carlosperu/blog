/*
 *
 * PostPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';import LoadingIndicator from 'components/LoadingIndicator';
import { loadPost } from './actions';
import { selectPost, selectTags, selectLoading, selectError } from './selectors';
import { selectSession } from '../App/selectors';
import Post from 'components/Post';

export class PostPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    this.props.loadPost(this.props.params.permalink, this.props.session);
  }

  render() {
    const { loading, error, post, tags, params, session } = this.props;
    let mainContent = null;

    // Show a loading indicator when we're loading
    if (loading) {
      mainContent = (<LoadingIndicator />);

    // Show an error if there is one
    } else if (error !== false) {
      mainContent = (<div>Something went wrong.</div>);

    // If we're not loading, don't have an error and there is a post, show the post
    } else if (params.permalink && post && post.tagIds) {
      mainContent = (<Post edit={session !== null} post={post} tags={tags} />);
    }
    return (
      <section>
        <Helmet
          title="PostPage"
          meta={[
            { name: 'description', content: 'Description of PostPage' },
          ]}
        />
        <div className="container">
          {mainContent}
        </div>
      </section>
    );
  }
}

PostPage.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  post: PropTypes.object,
  tags: PropTypes.object,
  session: PropTypes.object,
  loadPost: PropTypes.func,
  params: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  post: selectPost(),
  tags: selectTags(),
  loading: selectLoading(),
  error: selectError(),
  session: selectSession(),
});

export function mapDispatchToProps(dispatch) {
  return {
    loadPost: (permalink, session) => dispatch(loadPost(permalink, session)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PostPage);
