import { take, call, put, fork, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_POST } from './constants';
import { postLoaded, postLoadingError } from './actions';
import { sessionUpdated } from '../App/actions';
import { getRequest, getRequestBodyResponse, API_HOST } from 'utils/request';

/**
 * Github post request/response handler
 */
export function* getPost(permalink, session) {
  if (session && session['access-token']) {
    const requestURL = `${API_HOST}/v1/admin/posts/${permalink}`;

    try {
      // Call our request helper (see 'utils/request')
      const response = yield call(getRequest, requestURL, {
        headers: {
          'access-token': session['access-token'],
          'token-type': session['token-type'],
          client: session.client,
          expiry: session.expiry,
          uid: session.uid,
        },
      });

      // Build the tags array that will be pass to the state
      const tags = response.data.included ? response.data.included.reduce((reducedTags, tagItem) => {
        const newTags = reducedTags;
        newTags.ids.push(tagItem.id);
        const tag = {
          id: tagItem.id,
          name: tagItem.attributes.name,
          permalink: tagItem.attributes.permalink,
        };
        newTags[tagItem.id] = tag;
        return newTags;
      }, { ids: [] }) : [];
      const responsePost = response.data.data;

      // Build the post object that will be pass to the state
      const post = {
        id: responsePost.id,
        title: responsePost.attributes.title,
        content: responsePost.attributes.content,
        permalink: responsePost.attributes.permalink,
        publishedDate: responsePost.attributes['published-time'],
        published: responsePost.attributes.published,
        locale: responsePost.attributes.locale,
        tagIds: responsePost.relationships.tags.data.map((tag) => tag.id),
      };
      if (response.headers['access-token']) {
        const newSession = JSON.stringify({
          'access-token': response.headers['access-token'],
          'token-type': response.headers['token-type'],
          client: response.headers.client,
          expiry: response.headers.expiry,
          uid: response.headers.uid,
        });
        yield put(sessionUpdated(newSession));
      }
      yield put(postLoaded(post, tags));
    } catch (err) {
      yield put(postLoadingError(err));
    }
  } else {
    const requestURL = `${API_HOST}/v1/posts/${permalink}`;

    try {
      // Call our request helper (see 'utils/request')
      const response = yield call(getRequestBodyResponse, requestURL);

      // Build the tags array that will be pass to the state
      const tags = response.included ? response.included.reduce((reducedTags, tagItem) => {
        const newTags = reducedTags;
        newTags.ids.push(tagItem.id);
        const tag = {
          id: tagItem.id,
          name: tagItem.attributes.name,
          permalink: tagItem.attributes.permalink,
        };
        newTags[tagItem.id] = tag;
        return newTags;
      }, { ids: [] }) : [];
      const responsePost = response.data;

      // Build the post object that will be pass to the state
      const post = {
        id: responsePost.id,
        title: responsePost.attributes.title,
        content: responsePost.attributes.content,
        permalink: responsePost.attributes.permalink,
        published: responsePost.attributes.published,
        locale: responsePost.attributes.locale,
        publishedDate: responsePost.attributes['published-time'],
        tagIds: responsePost.relationships.tags.data.map((tag) => tag.id),
      };
      yield put(postLoaded(post, tags));
    } catch (err) {
      yield put(postLoadingError(err));
    }
  }
}

/**
 * Watches for LOAD_POST actions and calls getPost when one comes in.
 */
export function* getPostWatcher() {
  const { permalink, session } = yield take(LOAD_POST);
  yield fork(getPost, permalink, session);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* postData() {
  // Fork watcher so we can continue execution
  const watcher = yield fork(getPostWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// Bootstrap sagas
export default [
  postData,
];
