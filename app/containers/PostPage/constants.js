/*
 *
 * PostPage constants
 *
 */

 export const LOAD_POST = 'blog/PostPage/LOAD_POST';
 export const LOAD_POST_SUCCESS = 'blog/PostPage/LOAD_POST_SUCCESS';
 export const LOAD_POST_FAILURE = 'blog/PostPage/LOAD_POST_FAILURE';
