/*
 *
 * PostPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOAD_POST,
  LOAD_POST_SUCCESS,
  LOAD_POST_FAILURE,
} from './constants';

const initialState = fromJS({
  post: { },
  tags: { ids: [] },
  loading: false,
  error: false,
});

function postPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_POST:
      return state
        .set('loading', true);
    case LOAD_POST_SUCCESS:
      return state
        .set('loading', false)
        .set('error', false)
        .set('tags', action.tags)
        .set('post', action.post);
    case LOAD_POST_FAILURE:
      return state
        .set('loading', false)
        .set('error', true);
    default:
      return state;
  }
}

export default postPageReducer;
