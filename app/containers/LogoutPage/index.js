/*
 *
 * LogoutPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import LoadingIndicator from 'components/LoadingIndicator';
import { createStructuredSelector } from 'reselect';
import { selectSession } from '../App/selectors';
import { logout } from '../App/actions';

export class LogoutPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    this.props.logout(this.props.session);
  }

  render() {
    return (
      <section>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <LoadingIndicator />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

LogoutPage.propTypes = {
  session: PropTypes.object,
  logout: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  session: selectSession(),
});


export function mapDispatchToProps(dispatch) {
  return {
    logout: (session) => {
      dispatch(logout(session));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LogoutPage);
