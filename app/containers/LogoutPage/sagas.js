import { take, call, put, fork, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_LOGOUT } from '../App/constants';
import { sessionEnded, errorSigningOut } from '../App/actions';
import { deleteRequest, API_HOST } from 'utils/request';

/**
 * Github posts request/response handler
 */
export function* resetSession(session) {
  yield put(sessionEnded());
  const requestURL = `${API_HOST}/auth/sign_out`;
  try {
    // Call our request helper (see 'utils/request')
    yield call(deleteRequest, requestURL, {
      headers: {
        'access-token': session['access-token'],
        client: session.client,
        uid: session.uid,
      },
    });
  } catch (error) {
    yield put(errorSigningOut(error));
  }
}

/**
 * Watches for LOAD_LOGIN actions and calls getPosts when one comes in.
 */
export function* getSessionWatcher() {
  const { session } = yield take(LOAD_LOGOUT);
  yield fork(resetSession, session);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* sessionData() {
  // Fork watcher so we can continue execution
  const watcher = yield fork(getSessionWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// Bootstrap sagas
export default [
  sessionData,
];
