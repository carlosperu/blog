/*
 *
 * LoadingPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import LoadingIndicator from 'components/LoadingIndicator';

export class LoadingPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <section>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <LoadingIndicator />
            </div>
          </div>
        </div>
      </section>
    );
  }
}


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(null, mapDispatchToProps)(LoadingPage);
