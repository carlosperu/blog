/*
 *
 * LogoutLink
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { selectSession } from '../App/selectors';
import { createStructuredSelector } from 'reselect';
import { Link } from 'react-router';

export class LoggedUserLink extends React.Component { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { session, to, children } = this.props;
    if (session == null) {
      return null;
    }
    return (
      <Link to={to} >{children}</Link>
    );
  }
}

LoggedUserLink.propTypes = {
  to: PropTypes.string,
  children: PropTypes.object,
  session: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  session: selectSession(),
});


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoggedUserLink);
