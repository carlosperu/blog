/*
 * AboutPage Messages
 *
 * This contains all the text for the AboutPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'blog.containers.AboutPage.header',
    defaultMessage: 'About',
  },
  paragraph1: {
    id: 'blog.containers.AboutPage.paragraph1',
    defaultMessage: 'After living away from my home country for a decade, studying and working at the sillicon valley, traveling around many places in the world and making friends that come from different cultures, I developed a passion for computer and human languages.',
  },
  paragraph2: {
    id: 'blog.containers.AboutPage.paragraph2',
    defaultMessage: 'According to the diccionary, a polyglot is a person who can use several languages. In the software industry jargon, a polyglot is a person who can write code in multiple languages.',
  },
  paragraph3: {
    id: 'blog.containers.AboutPage.paragraph3',
    defaultMessage: 'Since I can speak fluently in Spanish and English, perform at a conversational level in Portugues and am able to achieve basic communication in French, I consider myself a Polyglot in human languages.',
  },
  paragraph4: {
    id: 'blog.containers.AboutPage.paragraph4',
    defaultMessage: 'In addition, I can code in Java, Ruby, JavaScript, C, C++ and to a lesser extend in PHP and Python. I\'m also familiar with markup languages such as CSS or HTML although they are not programming languages; they only describe presentation. Thus, you don\'t "code" in css or html. However, they are not less relevant or important because a fabulous and advanced application with a top-notch stack can be completely disregard by 99.999% of the world just due to a poor UI or UX.',
  },
});
