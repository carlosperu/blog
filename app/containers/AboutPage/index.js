/*
 *
 * AboutPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

export class AboutPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <section className="success" id="about">
        <Helmet
          title="About"
          meta={[
            { name: 'description', content: 'Explanation of why this website exists' },
          ]}
        />
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <h2>
                <FormattedMessage {...messages.header} />
              </h2>
              <hr className="star-light" />
            </div>
          </div>
          <div className="row">
            <div className="col-md-8 col-lg-offset-2">
              <p>
                <FormattedMessage {...messages.paragraph1} />
              </p>
            </div>
            <div className="col-md-8 col-lg-offset-2">
              <p>
                <FormattedMessage {...messages.paragraph2} />
              </p>
            </div>
            <div className="col-md-8 col-lg-offset-2">
              <p>
                <FormattedMessage {...messages.paragraph3} />
              </p>
            </div>
            <div className="col-md-8 col-lg-offset-2">
              <p>
                <FormattedMessage {...messages.paragraph4} />
              </p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(null, mapDispatchToProps)(AboutPage);
