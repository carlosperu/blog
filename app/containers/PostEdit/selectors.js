import { createSelector } from 'reselect';

/**
 * Direct selector to the postEdit state domain
 */
const selectPostEdit = () => (state) => state.get('postEdit');

/**
 * Default selector used by PostEdit
 */

const selectTags = () => createSelector(
  selectPostEdit(),
  (postEdit) => postEdit.get('tags'),
);

const selectTagsLoading = () => createSelector(
  selectPostEdit(),
  (postEdit) => postEdit.get('tagsLoading'),
);

const selectTagsError = () => createSelector(
  selectPostEdit(),
  (postEdit) => postEdit.get('tagsError'),
);

const selectPost = () => createSelector(
  selectPostEdit(),
  (postEdit) => postEdit.get('post'),
);

const selectPostLoading = () => createSelector(
  selectPostEdit(),
  (postEdit) => postEdit.get('postLoading'),
);

const selectPostError = () => createSelector(
  selectPostEdit(),
  (postEdit) => postEdit.get('postError'),
);

const selectSuccess = () => createSelector(
  selectPostEdit(),
  (postEdit) => postEdit.get('success'),
);

export {
  selectPostEdit,
  selectTags,
  selectTagsLoading,
  selectTagsError,
  selectPost,
  selectPostLoading,
  selectPostError,
  selectSuccess,
};
