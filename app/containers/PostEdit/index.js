/*
 *
 * PostEdit
 *
 */

import React, { PropTypes } from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { selectTags, selectTagsLoading, selectTagsError, selectPostLoading, selectPostError, selectSuccess } from './selectors';
import { selectLocale } from '../LanguageProvider/selectors';
import { selectSession } from '../App/selectors';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import PostEditForm from './PostEditForm';
import { loadPostAndTags, updatePost } from './actions';
import { checkSession } from '../App/actions';
import { routerActions } from 'react-router-redux';
import LoadingIndicator from 'components/LoadingIndicator';

export class PostEdit extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  componentWillMount() {
    this.props.checkSession(this.props.session);
    this.props.loadPostAndTags(this.props.params.postId, this.props.session);
  }

  handleUpdate(values) {
    return this.props.updatePost(values, this.props.tags, this.props.session);
  }

  render() {
    const { tagsLoading, postLoading, success, locale, tags } = this.props;
    let mainContent = null;
    if (tags == null || tagsLoading || postLoading) {
      mainContent = (<LoadingIndicator />);
    } else {
      const successMessage = success ? (
        <div className="row">
          <div className="col-md-6 col-md-offset-3">
            <h1><FormattedMessage {...messages.success} /></h1>
          </div>
        </div>) : null;
      mainContent = (
        <div className="container">
          {successMessage}
          <div className="row">
            <div className="col-md-6 col-md-offset-3">
              {/*
                handleUpdate will handle onSubmit.
                The locale prop is used to handle the locale inside the
                PostEditForm component.
                The tags props will be used by PostEditForm to display the tags
                 options available.
              */}
              <PostEditForm onSubmit={this.handleUpdate} locale={locale} tags={tags} />
            </div>
          </div>
          {successMessage}
        </div>);
    }
    return (
      <section>
        {mainContent}
      </section>
    );
  }
}

PostEdit.propTypes = {
  tags: PropTypes.object,
  tagsLoading: PropTypes.bool.isRequired,
  postLoading: PropTypes.bool.isRequired,
  locale: PropTypes.string.isRequired,
  session: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  success: PropTypes.bool,
  loadPostAndTags: PropTypes.func,
  checkSession: PropTypes.func,
  updatePost: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  tags: selectTags(),
  tagsLoading: selectTagsLoading(),
  tagsError: selectTagsError(),
  postLoading: selectPostLoading(),
  postError: selectPostError(),
  locale: selectLocale(),
  session: selectSession(),
  success: selectSuccess(),
});

export function mapDispatchToProps(dispatch) {
  return {
    loadPostAndTags: (postId, session) => dispatch(loadPostAndTags(postId, session)),
    checkSession: (session) => dispatch(checkSession(session)),
    updatePost: (values, tags, session) => new Promise((resolve, reject) => {
      dispatch(updatePost(values, tags, session, resolve, reject));
    }),
    postUpdated: (postId) => dispatch(routerActions.push(`/post/${postId}`)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PostEdit);
