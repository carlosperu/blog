/*
 * PostEdit Messages
 *
 * This contains all the text for the PostEdit component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  success: {
    id: 'blog.containers.PostEdit.success',
    defaultMessage: 'Successfully updated!',
  },
  title: {
    id: 'blog.containers.PostEdit.title',
    defaultMessage: 'Title',
  },
  content: {
    id: 'blog.containers.PostEdit.content',
    defaultMessage: 'Content',
  },
  published: {
    id: 'blog.containers.PostEdit.published',
    defaultMessage: 'Published',
  },
  locale: {
    id: 'blog.containers.PostEdit.locale',
    defaultMessage: 'Language',
  },
  tags: {
    id: 'blog.containers.PostEdit.tags',
    defaultMessage: 'Tags',
  },
  english: {
    id: 'blog.containers.PostEdit.english',
    defaultMessage: 'English',
  },
  spanish: {
    id: 'blog.containers.PostEdit.spanish',
    defaultMessage: 'Spanish',
  },
  update: {
    id: 'blog.containers.PostEdit.update',
    defaultMessage: 'update',
  },
});
