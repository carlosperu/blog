import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form/immutable';
import messages from './messages';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { selectPost } from './selectors';
import { createStructuredSelector } from 'reselect';

const RenderField = ({ input, type, meta: { touched, error } }) => (
  <div>
    <input {...input} className="form-control" type={type} />
    {touched && error && <span>{error}</span>}
  </div>
);

let Form = (props) => {
  const { error, handleSubmit, submitting, tags } = props;
  if (!tags || !tags.ids) return null;
  const tagsFormOptions = (
    <div>
      { tags.ids.map((id) => (
        <div key={id} className="form-group">
          <label htmlFor={id}>{tags[id].name}:</label>
          {/* Adds blank space */}
          {'  '.replace(/ /g, '\u00a0')}
          <Field name={id} type="checkbox" component="input" />
        </div>)
      )}
    </div>
  );
  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="title"><FormattedMessage {...messages.title} /></label>
        <Field name="title" type="text" component={RenderField} />
      </div>
      <div className="form-group">
        <label htmlFor="content"><FormattedMessage {...messages.content} /></label>
        <div>
          <Field name="content" className="form-control" rows="20" component="textarea" />
        </div>
      </div>
      <div className="form-group">
        <label htmlFor="published"><FormattedMessage {...messages.published} />:</label>
        {/* Adds blank space */}
        {'  '.replace(/ /g, '\u00a0')}
        <Field name="published" type="checkbox" component="input" />
      </div>
      <div className="form-group">
        <label htmlFor="locale"><FormattedMessage {...messages.locale} />:</label>
        {/* Adds blank space */}
        {'  '.replace(/ /g, '\u00a0')}
        <label htmlFor="locale"><Field name="locale" component="input" type="radio" value="en" /> <FormattedMessage {...messages.english} /></label>
        {/* Adds blank space */}
        {'  '.replace(/ /g, '\u00a0')}
        <label htmlFor="locale"><Field name="locale" component="input" type="radio" value="es" /> <FormattedMessage {...messages.spanish} /></label>
      </div>
      {tagsFormOptions}
      {error && <strong>{error}</strong>}
      <div>
        <button type="submit" className="btn btn-primary" disabled={submitting}><FormattedMessage {...messages.update} /></button>
      </div>
    </form>
  );
};

RenderField.propTypes = {
  input: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

Form.propTypes = {
  tags: PropTypes.object,
  handleSubmit: PropTypes.func.isRequired,
  error: PropTypes.string,
  submitting: PropTypes.bool.isRequired,
};

Form = reduxForm({
  form: 'postEditForm',  // a unique identifier for this form
})(Form);

const PostEditForm = connect(
   createStructuredSelector({
     initialValues: selectPost(),
   })
)(Form);

export default PostEditForm;
