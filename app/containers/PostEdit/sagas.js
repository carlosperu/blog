import { take, call, put, fork, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { UPDATE_POST, LOAD_POST_AND_TAGS } from './constants';
import { postUpdated, postUpdatingError, postLoaded, postLoadingError, tagsLoaded, tagsLoadingError } from './actions';
import { sessionUpdated } from '../App/actions';
import { patchRequest, getRequest, API_HOST } from 'utils/request';
import { SubmissionError } from 'redux-form';

/**
 * BlogApi posts request/response handler
 */
export function* updatePost(id, title, content, published, locale, tagIds, session, resolve, reject) {
  const requestURL = `${API_HOST}/v1/posts/${id}`;

  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(patchRequest, requestURL, {
      post: {
        title,
        content,
        published,
        locale,
        tag_ids: tagIds,
      },
    }, {
      headers: {
        'Content-Type': 'application/json',
        'access-token': session['access-token'],
        'token-type': session['token-type'],
        client: session.client,
        expiry: session.expiry,
        uid: session.uid,
      },
    });
    if (response.name && response.name === 'Error') {
      throw new SubmissionError({ _error: response.message });
    } else {
      const responsePost = response.data.data;
      const post = {
        id: responsePost.id,
        title: responsePost.attributes.title,
        content: responsePost.attributes.content,
        publishedDate: responsePost.attributes['published-time'],
        locale: responsePost.attributes.locale,
        published: responsePost.attributes.published,
      };
      responsePost.relationships.tags.data.forEach((tag) => {
        post[tag.id] = true;
      });
      const responseHeaders = response.headers;
      if (responseHeaders['access-token']) {
        const newSession = JSON.stringify({
          'access-token': responseHeaders['access-token'],
          'token-type': responseHeaders['token-type'],
          client: responseHeaders.client,
          expiry: responseHeaders.expiry,
          uid: responseHeaders.uid,
        });
        yield put(sessionUpdated(newSession));
      }
      yield put(postUpdated(post));
      yield call(resolve);
    }
  } catch (error) {
    yield call(reject, error);
    yield put(postUpdatingError(error));
  }
}

/**
 * Watches for UPDATE_POST actions and calls getPosts when one comes in.
 */
export function* getPostUpdateWatcher() {
  while (true) {
    const { id, title, content, published, locale, tagIds, session, resolve, reject } = yield take(UPDATE_POST);
    yield fork(updatePost, id, title, content, published, locale, tagIds, session, resolve, reject);
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export function* postUpdate() {
  // Fork watcher so we can continue execution
  const watcher = yield fork(getPostUpdateWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

/**
 * Github post request/response handler
 */
export function* getPost(postId, session) {
  const requestURL = `${API_HOST}/v1/admin/posts/${postId}`;

  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(getRequest, requestURL, {
      headers: {
        'access-token': session['access-token'],
        'token-type': session['token-type'],
        client: session.client,
        expiry: session.expiry,
        uid: session.uid,
      },
    });

    // Build the tags array that will be pass to the state
    const tags = response.data.included.reduce((reducedTags, tagItem) => {
      reducedTags.ids.push(tagItem.id);
      const tag = {
        id: tagItem.id,
        name: tagItem.attributes.name,
      };
      const newTags = reducedTags;
      newTags[tagItem.id] = tag;
      return newTags;
    }, { ids: [] });
    const responsePost = response.data.data;

    // Build the post object that will be pass to the state
    const post = {
      id: responsePost.id,
      title: responsePost.attributes.title,
      content: responsePost.attributes.content,
      publishedDate: responsePost.attributes['published-time'],
      locale: responsePost.attributes.locale,
      published: responsePost.attributes.published,
    };
    responsePost.relationships.tags.data.forEach((tag) => {
      post[tag.id] = true;
    });
    if (response.headers['access-token']) {
      const newSession = JSON.stringify({
        'access-token': response.headers['access-token'],
        'token-type': response.headers['token-type'],
        client: response.headers.client,
        expiry: response.headers.expiry,
        uid: response.headers.uid,
      });
      yield put(sessionUpdated(newSession));
    }
    yield put(postLoaded(post, tags));
  } catch (error) {
    yield put(postLoadingError(error));
  }
}


/**
 * BlogApi tags request/response handler
 */
export function* listTags(session) {
  const requestURL = `${API_HOST}/v1/tags`;

  try {
    // Call our request helper (see 'utils/request')
    const tagsResponse = yield call(getRequest, requestURL, {
      headers: {
        'access-token': session['access-token'],
        'token-type': session['token-type'],
        client: session.client,
        expiry: session.expiry,
        uid: session.uid,
      },
    });
    const tags = tagsResponse.data.data.reduce((reducedTags, tagItem) => {
      const tag = {
        id: tagItem.id,
        name: tagItem.attributes.name,
      };
      const newTags = reducedTags;
      newTags[tagItem.id] = tag;
      newTags.ids.push(tagItem.id);
      return newTags;
    }, { ids: [] });
    if (tagsResponse.headers['access-token']) {
      const newSession = JSON.stringify({
        'access-token': tagsResponse.headers['access-token'],
        'token-type': tagsResponse.headers['token-type'],
        client: tagsResponse.headers.client,
        expiry: tagsResponse.headers.expiry,
        uid: tagsResponse.headers.uid,
      });
      yield put(sessionUpdated(newSession));
    }
    yield put(tagsLoaded(tags));
  } catch (err) {
    yield put(tagsLoadingError(err));
  }
}

/**
 * Watches for LOAD_POST_AND_TAGS actions and calls getTags when one comes in.
 * By using `takeLatest` only the result of the latest API call is applied.
 */
export function* getPostAndTagsWatcher() {
  const { postId, session } = yield take(LOAD_POST_AND_TAGS);
  yield fork(getPost, postId, session);
  yield fork(listTags, session);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* postAndTagsData() {
  // Fork watcher so we can continue execution
  const watcher = yield fork(getPostAndTagsWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// Bootstrap sagas
export default [
  postUpdate,
  postAndTagsData,
];
