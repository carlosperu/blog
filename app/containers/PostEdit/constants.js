/*
 *
 * PostEdit constants
 *
 */

 export const LOAD_POST_AND_TAGS = 'blog/PostEdit/LOAD_POST_AND_TAGS';
 export const LOAD_POST_SUCCESS = 'blog/PostEdit/LOAD_POST_SUCCESS';
 export const LOAD_POST_FAILURE = 'blog/PostEdit/LOAD_POST_FAILURE';
 export const UPDATE_POST = 'blog/PostEdit/UPDATE_POST';
 export const UPDATE_POST_SUCCESS = 'blog/PostEdit/UPDATE_POST_SUCCESS';
 export const UPDATE_POST_FAILURE = 'blog/PostEdit/UPDATE_POST_FAILURE';
 export const LOAD_TAGS_SUCCESS = 'blog/PostEdit/LOAD_TAGS_SUCCESS';
 export const LOAD_TAGS_FAILURE = 'blog/PostEdit/LOAD_TAGS_FAILURE';
