/*
 *
 * PostEdit reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOAD_POST_AND_TAGS,
  LOAD_POST_SUCCESS,
  LOAD_POST_FAILURE,
  UPDATE_POST,
  UPDATE_POST_SUCCESS,
  UPDATE_POST_FAILURE,
  LOAD_TAGS_SUCCESS,
  LOAD_TAGS_FAILURE,
} from './constants';

const initialState = fromJS({
  tags: null,
  tagsLoading: false,
  tagsError: false,
  post: null,
  postLoading: false,
  postError: false,
  success: false,
});

function postEditReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_POST_AND_TAGS:
      return state
        .set('success', false)
        .set('postLoading', true)
        .set('tagsLoading', true);
    case UPDATE_POST:
      return state
        .set('success', false)
        .set('postLoading', true);
    case LOAD_POST_SUCCESS:
      return state
        .set('success', false)
        .set('post', action.post)
        .set('postLoading', false)
        .set('postError', false);
    case UPDATE_POST_SUCCESS:
      return state
        .set('success', true)
        .set('post', action.post)
        .set('postLoading', false)
        .set('postError', false);
    case LOAD_TAGS_SUCCESS:
      return state
        .set('tags', action.tags)
        .set('success', false)
        .set('tagsLoading', false)
        .set('tagsError', false);
    case UPDATE_POST_FAILURE:
    case LOAD_POST_FAILURE:
      return state
        .set('success', false)
        .set('postLoading', false)
        .set('postError', true);
    case LOAD_TAGS_FAILURE:
      return state
        .set('success', false)
        .set('tagsLoading', false)
        .set('tagsError', true);
    default:
      return state;
  }
}

export default postEditReducer;
