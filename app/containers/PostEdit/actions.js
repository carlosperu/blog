/*
 *
 * PostEdit actions
 *
 */

import {
  LOAD_POST_AND_TAGS,
  LOAD_POST_SUCCESS,
  LOAD_POST_FAILURE,
  UPDATE_POST,
  UPDATE_POST_SUCCESS,
  UPDATE_POST_FAILURE,
  LOAD_TAGS_SUCCESS,
  LOAD_TAGS_FAILURE,
} from './constants';

/**
 * Update a post, this action starts the request saga
 *
 * @return {object} An action object with a type of UPDATE_POST
 */
export function updatePost(values, tags, session, resolve, reject) {
  const id = values.get('id');
  const title = values.get('title');
  const content = values.get('content');
  const published = values.get('published');
  const locale = values.get('locale');
  const tagIds = tags.ids.filter((tagId) => values.get(tagId));
  return {
    type: UPDATE_POST,
    id,
    title,
    content,
    published,
    locale,
    tagIds,
    session,
    resolve,
    reject,
  };
}

/**
 * Dispatched when the post is created by the request saga
 *
 * @param  {object} post The post data
 *
 * @return {object}      An action object with a type of UPDATE_POST_SUCCESS passing the posts
 */
export function postUpdated(post) {
  return {
    type: UPDATE_POST_SUCCESS,
    post,
  };
}

/**
 * Dispatched when creating a post fails
 *
 * @param  {boolean} error The error
 *
 * @return {object}       An action object with a type of UPDATE_POST_FAILURE passing the error
 */
export function postUpdatingError(error) {
  return {
    type: UPDATE_POST_FAILURE,
    error,
  };
}

/**
 * Load the tags, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_TAGS
 */
export function loadPostAndTags(postId, session) {
  return {
    type: LOAD_POST_AND_TAGS,
    postId,
    session,
  };
}

/**
 * Dispatched when the tags are loaded by the request saga
 *
 * @param  {array} tags The tags data
 *
 * @return {object}      An action object with a type of LOAD_TAGS_SUCCESS passing the tags
 */
export function tagsLoaded(tags) {
  return {
    type: LOAD_TAGS_SUCCESS,
    tags,
  };
}

/**
 * Dispatched when loading the tags fails
 *
 * @param  {boolean} error The error
 *
 * @return {object}       An action object with a type of LOAD_TAGS_FAILURE passing the error
 */
export function tagsLoadingError(error) {
  return {
    type: LOAD_TAGS_FAILURE,
    error,
  };
}

/**
 * Dispatched when the post is loaded by the request saga
 *
 * @param  {object} posts The post data
 *
 * @param  {object} tags The tags data
 *
 * @return {object}      An action object with a type of LOAD_POST_SUCCESS passing the post
 */
export function postLoaded(post, tags) {
  return {
    type: LOAD_POST_SUCCESS,
    post,
    tags,
  };
}

/**
 * Dispatched when loading the post fails
 *
 * @param  {boolean} error The error
 *
 * @return {object}       An action object with a type of LOAD_POST_FAILURE passing the error
 */
export function postLoadingError(error) {
  return {
    type: LOAD_POST_FAILURE,
    error,
  };
}
