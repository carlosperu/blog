/*
 *
 * PostNew reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CREATE_POST,
  CREATE_POST_SUCCESS,
  CREATE_POST_FAILURE,
  LOAD_TAGS,
  LOAD_TAGS_SUCCESS,
  LOAD_TAGS_FAILURE,
} from './constants';

const initialState = fromJS({
  tags: { ids: [] },
  loading: false,
  error: false,
  post: null,
});

function postNewReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_POST:
    case LOAD_TAGS:
      return state
        .set('post', null)
        .set('loading', true);
    case CREATE_POST_SUCCESS:
      return state
        .set('post', action.post)
        .set('loading', false)
        .set('error', false);
    case LOAD_TAGS_SUCCESS:
      return state
        .set('tags', action.tags)
        .set('loading', false)
        .set('error', false);
    case CREATE_POST_FAILURE:
    case LOAD_TAGS_FAILURE:
      return state
        .set('loading', false)
        .set('error', true);
    default:
      return state;
  }
}

export default postNewReducer;
