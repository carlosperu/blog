/*
 *
 * PostNew constants
 *
 */

 export const CREATE_POST = 'blog/PostNew/CREATE_POST';
 export const CREATE_POST_SUCCESS = 'blog/PostNew/CREATE_POST_SUCCESS';
 export const CREATE_POST_FAILURE = 'blog/PostNew/CREATE_POST_FAILURE';
 export const LOAD_TAGS = 'blog/PostNew/LOAD_TAGS';
 export const LOAD_TAGS_SUCCESS = 'blog/PostNew/LOAD_TAGS_SUCCESS';
 export const LOAD_TAGS_FAILURE = 'blog/PostNew/LOAD_TAGS_FAILURE';
