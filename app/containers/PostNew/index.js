/*
 *
 * PostNew
 *
 */

import React, { PropTypes } from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { selectTags, selectPost } from './selectors';
import { selectLocale } from '../LanguageProvider/selectors';
import { selectSession } from '../App/selectors';
import PostNewForm from './PostNewForm';
import { loadTags, createPost } from './actions';
import { checkSession } from '../App/actions';
import { routerActions } from 'react-router-redux';

export class PostNew extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.handleCreate = this.handleCreate.bind(this);
  }

  componentWillMount() {
    this.props.checkSession(this.props.session);
    this.props.loadTags(this.props.session);
  }

  // shouldComponentUpdate(nextProps, nextState)
  componentWillReceiveProps({ postCreated, post }) {
    if (postCreated && post && post.id) {
      postCreated(post.id);
    }
  }

  handleCreate(values) {
    return this.props.createPost(values, this.props.tags, this.props.session);
  }

  render() {
    return (
      <section>
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-md-offset-3">
              {/*
                handleCreate will handle onSubmit.
                The locale prop is used to handle the locale inside the
                PostNewForm component.
                The tags props will be used by PostNewForm to display the tags
                 options available.
              */}
              <PostNewForm onSubmit={this.handleCreate} locale={this.props.locale} tags={this.props.tags} />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

PostNew.propTypes = {
  tags: PropTypes.object.isRequired,
  post: PropTypes.object,
  locale: PropTypes.string.isRequired,
  session: PropTypes.object.isRequired,
  loadTags: PropTypes.func,
  checkSession: PropTypes.func,
  createPost: PropTypes.func,
  postCreated: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  tags: selectTags(),
  post: selectPost(),
  locale: selectLocale(),
  session: selectSession(),
});

export function mapDispatchToProps(dispatch) {
  return {
    loadTags: (session) => dispatch(loadTags(session)),
    checkSession: (session) => dispatch(checkSession(session)),
    createPost: (values, tags, session) => new Promise((resolve, reject) => {
      dispatch(createPost(values, tags, session, resolve, reject));
    }),
    postCreated: (postId) => dispatch(routerActions.push(`/post/${postId}`)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PostNew);
