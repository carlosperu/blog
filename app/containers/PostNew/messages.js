/*
 * PostNew Messages
 *
 * This contains all the text for the PostNew component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'blog.containers.PostNew.title',
    defaultMessage: 'Title',
  },
  content: {
    id: 'blog.containers.PostNew.content',
    defaultMessage: 'Content',
  },
  published: {
    id: 'blog.containers.PostNew.published',
    defaultMessage: 'Published',
  },
  locale: {
    id: 'blog.containers.PostNew.locale',
    defaultMessage: 'Language',
  },
  tags: {
    id: 'blog.containers.PostNew.tags',
    defaultMessage: 'Tags',
  },
  english: {
    id: 'blog.containers.PostNew.english',
    defaultMessage: 'English',
  },
  spanish: {
    id: 'blog.containers.PostNew.spanish',
    defaultMessage: 'Spanish',
  },
  create: {
    id: 'blog.containers.PostNew.create',
    defaultMessage: 'Create',
  },
});
