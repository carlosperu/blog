import { take, call, put, fork, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { CREATE_POST, LOAD_TAGS } from './constants';
import { postCreated, postCreatingError, tagsLoaded, tagsLoadingError } from './actions';
import { sessionUpdated } from '../App/actions';
import { postRequest, getRequest, API_HOST } from 'utils/request';
import { SubmissionError } from 'redux-form';

/**
 * BlogApi posts request/response handler
 */
export function* createPost(title, content, published, locale, tagIds, session, resolve, reject) {
  const requestURL = `${API_HOST}/v1/posts`;

  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(postRequest, requestURL, {
      post: {
        title,
        content,
        published,
        locale,
        tag_ids: tagIds,
      },
    }, {
      headers: {
        'Content-Type': 'application/json',
        'access-token': session['access-token'],
        'token-type': session['token-type'],
        client: session.client,
        expiry: session.expiry,
        uid: session.uid,
      },
    });
    if (response.name && response.name === 'Error') {
      throw new SubmissionError({ _error: response.message });
    } else {
      const responsePost = response.data.data;
      const post = {
        id: responsePost.id,
        title: responsePost.attributes.title,
        content: responsePost.attributes.content,
        publishedDate: responsePost.attributes['published-time'],
        published: responsePost.attributes.published,
        locale: responsePost.attributes.locale,
        tagIds: responsePost.relationships.tags.data.map((tag) => tag.id),
      };
      const responseHeaders = response.headers;
      if (responseHeaders['access-token']) {
        const newSession = JSON.stringify({
          'access-token': responseHeaders['access-token'],
          'token-type': responseHeaders['token-type'],
          client: responseHeaders.client,
          expiry: responseHeaders.expiry,
          uid: responseHeaders.uid,
        });
        yield put(sessionUpdated(newSession));
      }
      yield put(postCreated(post));
      yield call(resolve);
    }
  } catch (error) {
    yield call(reject, error);
    yield put(postCreatingError(error));
  }
}

/**
 * Watches for CREATE_POST actions and calls getPosts when one comes in.
 * By using `takeLatest` only the result of the latest API call is applied.
 */
export function* getPostWatcher() {
  while (true) {
    const { title, content, published, locale, tagIds, session, resolve, reject } = yield take(CREATE_POST);
    yield fork(createPost, title, content, published, locale, tagIds, session, resolve, reject);
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export function* postCreation() {
  // Fork watcher so we can continue execution
  const watcher = yield fork(getPostWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

/**
 * BlogApi tags request/response handler
 */
export function* listTags(session) {
  const requestURL = `${API_HOST}/v1/tags`;

  try {
    // Call our request helper (see 'utils/request')
    const tagsResponse = yield call(getRequest, requestURL, {
      headers: {
        'access-token': session['access-token'],
        'token-type': session['token-type'],
        client: session.client,
        expiry: session.expiry,
        uid: session.uid,
      },
    });
    const tags = tagsResponse.data.data.reduce((reducedTags, tagItem) => {
      const tag = {
        id: tagItem.id,
        name: tagItem.attributes.name,
      };
      const newTags = reducedTags;
      newTags[tagItem.id] = tag;
      newTags.ids.push(tagItem.id);
      return newTags;
    }, { ids: [] });
    if (tagsResponse.headers['access-token']) {
      const newSession = JSON.stringify({
        'access-token': tagsResponse.headers['access-token'],
        'token-type': tagsResponse.headers['token-type'],
        client: tagsResponse.headers.client,
        expiry: tagsResponse.headers.expiry,
        uid: tagsResponse.headers.uid,
      });
      yield put(sessionUpdated(newSession));
    }
    yield put(tagsLoaded(tags));
  } catch (err) {
    yield put(tagsLoadingError(err));
  }
}

/**
 * Watches for LOAD_TAGS actions and calls getTags when one comes in.
 * By using `takeLatest` only the result of the latest API call is applied.
 */
export function* getTagsWatcher() {
  const { session } = yield take(LOAD_TAGS);
  yield fork(listTags, session);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* tagsData() {
  // Fork watcher so we can continue execution
  const watcher = yield fork(getTagsWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// Bootstrap sagas
export default [
  postCreation,
  tagsData,
];
