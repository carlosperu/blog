import { createSelector } from 'reselect';

/**
 * Direct selector to the postNew state
 */
const selectPostNew = () => (state) => state.get('postNew');

/**
 * Default selector used by PostNew
 */

const selectTags = () => createSelector(
  selectPostNew(),
  (postNew) => postNew.get('tags'),
);

const selectLoading = () => createSelector(
  selectPostNew(),
  (postNew) => postNew.get('loading'),
);

const selectError = () => createSelector(
  selectPostNew(),
  (postNew) => postNew.get('error'),
);

const selectPost = () => createSelector(
  selectPostNew(),
  (postNew) => postNew.get('post'),
);

export {
  selectPostNew,
  selectTags,
  selectLoading,
  selectError,
  selectPost,
};
