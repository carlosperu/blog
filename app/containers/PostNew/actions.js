/*
 *
 * PostNew actions
 *
 */

import {
  CREATE_POST,
  CREATE_POST_SUCCESS,
  CREATE_POST_FAILURE,
  LOAD_TAGS,
  LOAD_TAGS_SUCCESS,
  LOAD_TAGS_FAILURE,
} from './constants';

/**
 * Create a post, this action starts the request saga
 *
 * @return {object} An action object with a type of CREATE_POST
 */
export function createPost(values, tags, session, resolve, reject) {
  const title = values.get('title');
  const content = values.get('content');
  const published = values.get('published');
  const locale = values.get('locale');
  const tagIds = tags.ids.filter((id) => values.get(id));
  return {
    type: CREATE_POST,
    title,
    content,
    published,
    locale,
    tagIds,
    session,
    resolve,
    reject,
  };
}

/**
 * Dispatched when the post is created by the request saga
 *
 * @param  {object} post The post data
 *
 * @return {object}      An action object with a type of CREATE_POST_SUCCESS passing the posts
 */
export function postCreated(post) {
  return {
    type: CREATE_POST_SUCCESS,
    post,
  };
}

/**
 * Dispatched when creating a post fails
 *
 * @param  {boolean} error The error
 *
 * @return {object}       An action object with a type of CREATE_POST_FAILURE passing the error
 */
export function postCreatingError(error) {
  return {
    type: CREATE_POST_FAILURE,
    error,
  };
}

/**
 * Load the tags, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_TAGS
 */
export function loadTags(session) {
  return {
    type: LOAD_TAGS,
    session,
  };
}

/**
 * Dispatched when the tags are loaded by the request saga
 *
 * @param  {array} tags The tags data
 *
 * @return {object}      An action object with a type of LOAD_TAGS_SUCCESS passing the tags
 */
export function tagsLoaded(tags) {
  return {
    type: LOAD_TAGS_SUCCESS,
    tags,
  };
}

/**
 * Dispatched when loading the tags fails
 *
 * @param  {boolean} error The error
 *
 * @return {object}       An action object with a type of LOAD_TAGS_FAILURE passing the error
 */
export function tagsLoadingError(error) {
  return {
    type: LOAD_TAGS_FAILURE,
    error,
  };
}
