// import expect from 'expect';
//
// import {
//   LOAD_LOGIN,
//   LOAD_LOGIN_SUCCESS,
//   LOAD_LOGIN_FAILURE,
//   LOAD_LOGOUT,
//   LOAD_LOGOUT_SUCCESS,
//   LOAD_LOGOUT_FAILURE,
//   CHECK_SESSION,
//   UPDATE_SESSION,
//   TOGGLE_MENU,
//   CLOSE_MENU,
// } from '../constants';
//
// import {
//   toggleMenu,
//   closeMenu,
//   checkSession,
//   toggleMenu,
//   closeMenu,
//   checkSession,
// } from '../actions';
//
// describe('App Actions', () => {
//   describe('loadRepos', () => {
//     it('should return the correct type', () => {
//       const expectedResult = {
//         type: LOAD_REPOS,
//       };
//
//       expect(loadRepos()).toEqual(expectedResult);
//     });
//   });
//
//   describe('reposLoaded', () => {
//     it('should return the correct type and the passed repos', () => {
//       const fixture = ['Test'];
//       const username = 'test';
//       const expectedResult = {
//         type: LOAD_REPOS_SUCCESS,
//         repos: fixture,
//         username,
//       };
//
//       expect(reposLoaded(fixture, username)).toEqual(expectedResult);
//     });
//   });
//
//   describe('repoLoadingError', () => {
//     it('should return the correct type and the error', () => {
//       const fixture = {
//         msg: 'Something went wrong!',
//       };
//       const expectedResult = {
//         type: LOAD_REPOS_ERROR,
//         error: fixture,
//       };
//
//       expect(repoLoadingError(fixture)).toEqual(expectedResult);
//     });
//   });
// });
