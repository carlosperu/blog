/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const LOAD_LOGIN = 'blog/App/LOAD_LOGIN';
export const LOAD_LOGIN_SUCCESS = 'blog/App/LOAD_LOGIN_SUCCESS';
export const LOAD_LOGIN_FAILURE = 'blog/App/LOAD_LOGIN_FAILURE';
export const LOAD_LOGOUT = 'blog/App/LOAD_LOGOUT';
export const LOAD_LOGOUT_SUCCESS = 'blog/App/LOAD_LOGOUT_SUCCESS';
export const LOAD_LOGOUT_FAILURE = 'blog/App/LOAD_LOGOUT_FAILURE';
export const CHECK_SESSION = 'blog/App/CHECK_SESSION';
export const UPDATE_SESSION = 'blog/App/UPDATE_SESSION';
export const TOGGLE_MENU = 'blog/App/TOGGLE_MENU';
export const CLOSE_MENU = 'blog/App/CLOSE_MENU';
