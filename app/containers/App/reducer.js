/*
 *
 * LoginPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOAD_LOGIN,
  LOAD_LOGIN_SUCCESS,
  LOAD_LOGIN_FAILURE,
  LOAD_LOGOUT,
  LOAD_LOGOUT_SUCCESS,
  LOAD_LOGOUT_FAILURE,
  CHECK_SESSION,
  UPDATE_SESSION,
  TOGGLE_MENU,
  CLOSE_MENU,
} from './constants';

const initialState = fromJS({
  session: localStorage.getItem('session') || null,
  loading: false,
  error: false,
  collapse: true,
});

function appReducer(state = initialState, { type, session }) {
  switch (type) {
    case TOGGLE_MENU: {
      const collapseState = state.get('collapse');
      return state
        .set('collapse', !collapseState);
    }
    case CLOSE_MENU:
      return state
        .set('collapse', true);
    case LOAD_LOGIN:
    case LOAD_LOGOUT:
      return state
        .set('loading', true);
    case LOAD_LOGIN_SUCCESS:
      return state
        .set('session', session)
        .set('loading', false)
        .set('error', false);
    case UPDATE_SESSION:
      // Update local storage to persist the session
      localStorage.setItem('session', session);
      return state
        .set('session', session)
        .set('loading', false)
        .set('error', false);
    case LOAD_LOGOUT_SUCCESS:
      // Remove session from local storage
      localStorage.removeItem('session');
      return state
        .set('session', null)
        .set('loading', false)
        .set('error', false);
    case LOAD_LOGIN_FAILURE:
    case LOAD_LOGOUT_FAILURE:
      return state
        .set('loading', false)
        .set('error', true);
    case CHECK_SESSION:
    default:
      return state;
  }
}

export default appReducer;
