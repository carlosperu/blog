/*
 *
 * App actions
 *
 */

import {
  LOAD_LOGIN,
  LOAD_LOGIN_SUCCESS,
  LOAD_LOGIN_FAILURE,
  LOAD_LOGOUT,
  LOAD_LOGOUT_SUCCESS,
  LOAD_LOGOUT_FAILURE,
  CHECK_SESSION,
  UPDATE_SESSION,
  TOGGLE_MENU,
  CLOSE_MENU,
} from './constants';

/**
 * Toggle menu, this action anounce the toggling
 *
 * @return {object} An action object with a type of TOGGLE_MENU
 */
export function toggleMenu() {
  return {
    type: TOGGLE_MENU,
  };
}

/**
 * Close menu, this action anounce the closing
 *
 * @return {object} An action object with a type of CLOSE_MENU
 */
export function closeMenu() {
  return {
    type: CLOSE_MENU,
  };
}

/**
 * Dispatched when a session needs to be checked by the request saga
 *
 * @param  {object} session The session object
 *
 * @return {object}      An action object with a type of CHECK_SESSION passing the session
 */
export function checkSession(session) {
    // Check the current time against the session expiry key.
    // If the current time is later (greater) then expiry, the session has ended.
  if (session && Math.floor(Date.now() / 1000) > session.expiry) {
    sessionEnded();
  }
  return {
    type: CHECK_SESSION,
  };
}

/**
 * Signs in, this action starts the request saga
 *
 * @param  {string} email The user email
 *
 * @param  {string} password The user password
 *
 * @return {object} An action object with a type of LOAD_LOGIN
 */
export function login(email, password, resolve, reject) {
  return {
    type: LOAD_LOGIN,
    email,
    password,
    resolve,
    reject,
  };
}

/**
 * Dispatched when the session has updated by the request saga
 *
 * @param  {object} session The session object
 *
 * @return {object}      An action object with a type of UPDATE_SESSION passing the session
 */
export function sessionUpdated(session) {
  return {
    type: UPDATE_SESSION,
    session,
  };
}

/**
 * Dispatched when the session has begun by the request saga
 *
 * @param  {object} session The session object
 *
 * @return {object}      An action object with a type of LOAD_LOGIN_SUCCESS passing the session
 */
export function sessionBegun(session) {
  return {
    type: LOAD_LOGIN_SUCCESS,
    session,
  };
}

/**
 * Dispatched when signing in fails
 *
 * @param  {boolean} error The error
 *
 * @return {object}       An action object with a type of LOAD_LOGIN_FAILURE passing the error
 */
export function errorSigningIn(error) {
  return {
    type: LOAD_LOGIN_FAILURE,
    error,
  };
}

/**
 * Signs out, this action starts the ending of the session
 *
 * @param  {object} session The user session
 *
 * @return {object} An action object with a type of LOAD_LOGIN
 */
export function logout(session) {
  return {
    type: LOAD_LOGOUT,
    session,
  };
}

/**
 * Dispatched when the session has ended by the request saga
 *
 * @return {object}      An action object with a type of LOAD_LOGOUT_SUCCESS passing the session
 */
export function sessionEnded() {
  return {
    type: LOAD_LOGOUT_SUCCESS,
    session: null,
  };
}

/**
 * Dispatched when signing out fails
 *
 * @param  {boolean} error The error
 *
 * @return {object}       An action object with a type of LOAD_LOGOUT_FAILURE passing the error
 */
export function errorSigningOut(error) {
  return {
    type: LOAD_LOGOUT_FAILURE,
    error,
  };
}
