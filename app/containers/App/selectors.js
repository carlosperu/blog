/**
 * The global state selectors
 */

import { createSelector } from 'reselect';

const selectAuth = () => (state) => state.get('auth');

const selectSession = () => createSelector(
  selectAuth(),
  (authState) => {
    // TODO: Simplify this selector
    const session = authState.get('session') && authState.get('session') !== '{}' ? JSON.parse(authState.get('session')) : null;
    return session;
  }
);

const selectLoading = () => createSelector(
  selectAuth(),
  (authState) => authState.get('loading')
);

const selectError = () => createSelector(
  selectAuth(),
  (authState) => authState.get('error')
);

const selectCollapse = () => createSelector(
  selectAuth(),
  (authState) => authState.get('collapse'),
);

// selectLocationState expects a plain JS object for the routing state
const selectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route'); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

export {
  selectAuth,
  selectSession,
  selectLoading,
  selectError,
  selectLocationState,
  selectCollapse,
};
