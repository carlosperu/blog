/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  notFound: {
    id: 'blog.components.NotFoundPage.notFound',
    defaultMessage: 'There is nothing here. Are you sure you want to be here?',
  },
});
