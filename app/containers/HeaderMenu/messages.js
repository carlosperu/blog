/*
 * Header Messages
 *
 * This contains all the text for the Header component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  new: {
    id: 'blog.components.Header.new',
    defaultMessage: 'New',
  },
  blog: {
    id: 'blog.components.Header.blog',
    defaultMessage: 'Blog',
  },
  about: {
    id: 'blog.components.Header.about',
    defaultMessage: 'About',
  },
  logout: {
    id: 'blog.containers.Header.logout',
    defaultMessage: 'Logout',
  },
  dashboard: {
    id: 'blog.containers.Header.dashboard',
    defaultMessage: 'Dashboard',
  },
});
