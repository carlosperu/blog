/*
 *
 * HeaderMenu
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { selectCollapse } from '../App/selectors';
import { createStructuredSelector } from 'reselect';
import { toggleMenu, closeMenu } from '../App/actions';
import { Link } from 'react-router';
import LocaleToggle from 'containers/LocaleToggle';
import LoggedUserLink from 'containers/LoggedUserLink';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

export class HeaderMenu extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const collapseClass = this.props.collapse ? 'collapse navbar-collapse' : 'navbar-collapse';
    return (
      <div className="container">
        <div className="navbar-header page-scroll">
          <button type="button" className="navbar-toggle" data-toggle="collapse" onClick={this.props.toggleMenu} onBlur={this.props.closeMenu} data-target="#bs-example-navbar-collapse-1">
            <span className="sr-only">Toggle navigation</span> Menu <i className="fa fa-bars"></i>
          </button>
          <Link className="navbar-brand" to="/">Polyglot Carlos</Link>
        </div>

        <div className={collapseClass} id="bs-example-navbar-collapse-1">
          <ul className="nav navbar-nav navbar-right">
            <li>
              <LoggedUserLink to="/postNew" ><FormattedMessage {...messages.new} /></LoggedUserLink>
            </li>
            <li>
              <Link to="/"><FormattedMessage {...messages.blog} /></Link>
            </li>
            <li>
              <Link to="/about"><FormattedMessage {...messages.about} /></Link>
            </li>
            <li>
              <LocaleToggle />
            </li>
            <li>
              <LoggedUserLink to="/caletaza" ><FormattedMessage {...messages.dashboard} /></LoggedUserLink>
            </li>
            <li>
              <LoggedUserLink to="/logout" ><FormattedMessage {...messages.logout} /></LoggedUserLink>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

HeaderMenu.propTypes = {
  collapse: PropTypes.bool.isRequired,
  toggleMenu: PropTypes.func.isRequired,
  closeMenu: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  collapse: selectCollapse(),
});

export function mapDispatchToProps(dispatch) {
  return {
    toggleMenu: () => dispatch(toggleMenu()),
    closeMenu: () => dispatch(closeMenu()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMenu);
