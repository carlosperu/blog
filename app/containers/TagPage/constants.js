/*
 *
 * TagPage constants
 *
 */

 export const LOAD_TAG_POSTS = 'blog/TagPage/LOAD_TAG_POSTS';
 export const LOAD_TAG_POSTS_SUCCESS = 'blog/TagPage/LOAD_TAG_POSTS_SUCCESS';
 export const LOAD_TAG_POSTS_FAILURE = 'blog/TagPage/LOAD_TAG_POSTS_FAILURE';
