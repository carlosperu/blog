import { take, call, put, fork, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_TAG_POSTS } from './constants';
import { tagPostsLoaded, tagPostsLoadingError } from './actions';
import { getRequestBodyResponse, API_HOST } from 'utils/request';

/**
 * Github posts request/response handler
 */
export function* getTagPosts(permalink, locale) {
  const requestURL = `${API_HOST}/v1/tags/${permalink}?locale=${locale}`;

  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(getRequestBodyResponse, requestURL);
    const tag = {
      id: response.data[0].id,
      name: response.data[0].attributes.name,
    };

    const posts = response.included.reduce((reducedPosts, postItem) => {
      const post = {
        id: postItem.id,
        title: postItem.attributes.title,
        content: postItem.attributes.content,
        permalink: postItem.attributes.permalink,
        publishedDate: postItem.attributes['published-time'],
      };
      const newPosts = reducedPosts;
      newPosts[postItem.id] = post;
      newPosts.ids.push(postItem.id);
      return newPosts;
    }, { ids: [] });
    yield put(tagPostsLoaded(tag, posts));
  } catch (err) {
    yield put(tagPostsLoadingError(err));
  }
}

/**
 * Watches for LOAD_TAG_POSTS actions and calls getTagPosts when one comes in.
 */
export function* getTagPostsWatcher() {
  const { permalink, locale } = yield take(LOAD_TAG_POSTS);
  yield fork(getTagPosts, permalink, locale);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* tagPostsData() {
  // Fork watcher so we can continue execution
  const watcher = yield fork(getTagPostsWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// Bootstrap sagas
export default [
  tagPostsData,
];
