import { createSelector } from 'reselect';

/**
 * Direct selector to the tagPage state domain
 */
const selectTagPage = () => (state) => state.get('tagPage');

/**
 * Default selector used by TagPage
 */

const selectTag = () => createSelector(
  selectTagPage(),
  (tagState) => tagState.get('tag'),
);

const selectPosts = () => createSelector(
  selectTagPage(),
  (tagState) => tagState.get('posts'),
);

const selectLoading = () => createSelector(
  selectTagPage(),
  (tagState) => tagState.get('loading'),
);

const selectError = () => createSelector(
  selectTagPage(),
  (tagState) => tagState.get('error'),
);

export {
  selectTagPage,
  selectTag,
  selectPosts,
  selectLoading,
  selectError,
};
