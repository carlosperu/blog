/*
 *
 * TagPage actions
 *
 */

import {
  LOAD_TAG_POSTS,
  LOAD_TAG_POSTS_SUCCESS,
  LOAD_TAG_POSTS_FAILURE,
} from './constants';

/**
 * Load the posts, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_POSTS
 */
export function loadTagPosts(permalink, locale) {
  return {
    type: LOAD_TAG_POSTS,
    permalink,
    locale,
  };
}

/**
 * Dispatched when the posts are loaded by the request saga
 *
 * @param  {object} tag  The tag data
 * @param  {object} posts The posts data
 *
 * @return {object}      An action object with a type of LOAD_POSTS_SUCCESS passing the posts
 */
export function tagPostsLoaded(tag, posts) {
  return {
    type: LOAD_TAG_POSTS_SUCCESS,
    tag,
    posts,
  };
}

/**
 * Dispatched when loading the posts fails
 *
 * @param  {boolean} error The error
 *
 * @return {object}       An action object with a type of LOAD_POSTS_FAILURE passing the error
 */
export function tagPostsLoadingError(error) {
  return {
    type: LOAD_TAG_POSTS_FAILURE,
    error,
  };
}
