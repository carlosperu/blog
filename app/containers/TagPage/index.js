/*
 *
 * TagPage
 *
 */

import React, { PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import PostList from 'components/PostList';
import { createStructuredSelector } from 'reselect';
import LoadingIndicator from 'components/LoadingIndicator';
import { loadTagPosts } from './actions';
import List from 'components/List';
import ListItem from 'components/ListItem';
import { selectTag, selectPosts, selectLoading, selectError } from './selectors';
import { selectLocale } from '../LanguageProvider/selectors';

export class TagPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    this.props.loadTagPosts(this.props.params.permalink, this.props.locale);
  }

  render() {
    const { loading, error, posts, tag, params } = this.props;
    let title = null;
    let mainContent = null;

    // Show a loading indicator when we're loading
    if (loading) {
      mainContent = (<LoadingIndicator />);

    // Show an error if there is one
    } else if (error !== false) {
      const ErrorComponent = () => (
        <ListItem item={'Something went wrong, please try again!'} />
      );
      mainContent = (<List component={ErrorComponent} />);

      // If we're not loading, don't have an error and there are posts, show the posts
    } else if (params.permalink && tag && tag.name) {
      title = tag.name;
      mainContent = (<PostList posts={posts} />);
    }
    return (
      <section>
        <Helmet
          title="Blog"
          meta={[
            { name: 'description', content: 'This blog is about all topics that interest me' },
          ]}
        />
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <h2>{title}</h2>
              <hr className="star-primary" />
            </div>
          </div>
          {mainContent}
        </div>
      </section>);
  }
}

TagPage.propTypes = {
  tag: PropTypes.object.isRequired,
  posts: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  locale: PropTypes.string.isRequired,
  loadTagPosts: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  tag: selectTag(),
  posts: selectPosts(),
  loading: selectLoading(),
  error: selectError(),
  locale: selectLocale(),
});

export function mapDispatchToProps(dispatch) {
  return {
    loadTagPosts: (permalink, locale) => dispatch(loadTagPosts(permalink, locale)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TagPage);
