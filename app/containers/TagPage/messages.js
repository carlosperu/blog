/*
 * TagPage Messages
 *
 * This contains all the text for the TagPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'blog.containers.TagPage.header',
    defaultMessage: 'This is TagPage container !',
  },
});
