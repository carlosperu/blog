/*
 *
 * TagPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOAD_TAG_POSTS,
  LOAD_TAG_POSTS_SUCCESS,
  LOAD_TAG_POSTS_FAILURE,
} from 'containers/TagPage/constants';

const initialState = fromJS({
  posts: { ids: [] },
  tag: { },
  loading: false,
  error: false,
});

function tagPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_TAG_POSTS:
      return state
        .set('loading', true);
    case LOAD_TAG_POSTS_SUCCESS:
      return state
        .set('posts', action.posts)
        .set('tag', action.tag)
        .set('loading', false)
        .set('error', false);
    case LOAD_TAG_POSTS_FAILURE:
      return state
        .set('loading', false)
        .set('error', true);
    default:
      return state;
  }
}

export default tagPageReducer;
