import { take, call, put, fork, cancel } from 'redux-saga/effects';
import { LOCATION_CHANGE } from 'react-router-redux';
import { LOAD_TAGS, LOAD_POSTS } from './constants';
import { postsLoaded, postsLoadingError, tagsLoaded, tagsLoadingError } from './actions';
import { sessionUpdated } from '../App/actions';
import { getRequest, API_HOST } from 'utils/request';

/**
 * BlogApi tags request/response handler
 */
export function* listTags(session) {
  const requestURL = `${API_HOST}/v1/tags`;

  try {
    // Call our request helper (see 'utils/request')
    const tagsResponse = yield call(getRequest, requestURL, {
      headers: {
        'access-token': session['access-token'],
        'token-type': session['token-type'],
        client: session.client,
        expiry: session.expiry,
        uid: session.uid,
      },
    });
    const tags = tagsResponse.data.data.reduce((reducedTags, tagItem) => {
      const tag = {
        id: tagItem.id,
        name: tagItem.attributes.name,
      };
      const newTags = reducedTags;
      newTags[tagItem.id] = tag;
      newTags.ids.push(tagItem.id);
      return newTags;
    }, { ids: [] });
    if (tagsResponse.headers['access-token']) {
      const newSession = JSON.stringify({
        'access-token': tagsResponse.headers['access-token'],
        'token-type': tagsResponse.headers['token-type'],
        client: tagsResponse.headers.client,
        expiry: tagsResponse.headers.expiry,
        uid: tagsResponse.headers.uid,
      });
      yield put(sessionUpdated(newSession));
    } else if (tags && tags.ids && tags.ids.length > 0) {
      yield put(sessionUpdated(JSON.stringify(session)));
    }
    yield put(tagsLoaded(tags));
  } catch (err) {
    yield put(tagsLoadingError(err));
  }
}

/**
 * Watches for LOAD_TAGS actions and calls getTags when one comes in.
 */
export function* getTagsWatcher() {
  const { session } = yield take(LOAD_TAGS);
  yield fork(listTags, session);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* tagsData() {
  // Fork watcher so we can continue execution
  const watcher = yield fork(getTagsWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}/**
 * BlogApi posts request/response handler
 */
export function* listPosts(session) {
  const requestURL = `${API_HOST}/v1/admin/posts`;

  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(getRequest, requestURL, {
      headers: {
        'access-token': session['access-token'],
        'token-type': session['token-type'],
        client: session.client,
        expiry: session.expiry,
        uid: session.uid,
      },
    });
    const posts = response.data.data.reduce((reducedPosts, postItem) => {
      const post = {
        id: postItem.id,
        title: postItem.attributes.title,
        content: postItem.attributes.content,
        permalink: postItem.attributes.permalink,
        publishedDate: postItem.attributes['published-time'],
      };
      const newPosts = reducedPosts;
      newPosts[postItem.id] = post;
      newPosts.ids.push(postItem.id);
      return newPosts;
    }, { ids: [] });
    if (response.headers['access-token']) {
      const newSession = JSON.stringify({
        'access-token': response.headers['access-token'],
        'token-type': response.headers['token-type'],
        client: response.headers.client,
        expiry: response.headers.expiry,
        uid: response.headers.uid,
      });
      yield put(sessionUpdated(newSession));
    }
    yield put(postsLoaded(posts));
  } catch (err) {
    yield put(postsLoadingError(err));
  }
}

/**
 * Watches for LOAD_POSTS actions and calls getPosts when one comes in.
 */
export function* getPostsWatcher() {
  const { session } = yield take(LOAD_POSTS);
  yield fork(listPosts, session);
}

/**
 * Root saga manages watcher lifecycle
 */
export function* postsData() {
  // Fork watcher so we can continue execution
  const watcher = yield fork(getPostsWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// Bootstrap sagas
export default [
  postsData,
  tagsData,
];
