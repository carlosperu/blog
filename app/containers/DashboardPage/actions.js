/*
 *
 * DashboardPage actions
 *
 */

import {
  LOAD_POSTS,
  LOAD_POSTS_SUCCESS,
  LOAD_POSTS_FAILURE,
  LOAD_TAGS,
  LOAD_TAGS_SUCCESS,
  LOAD_TAGS_FAILURE,
} from './constants';

/**
 * Load the tags, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_TAGS
 */
export function loadTags(session) {
  return {
    type: LOAD_TAGS,
    session,
  };
}

/**
 * Dispatched when the tags are loaded by the request saga
 *
 * @param  {array} tags The tags data
 *
 * @return {object}      An action object with a type of LOAD_TAGS_SUCCESS passing the tags
 */
export function tagsLoaded(tags) {
  return {
    type: LOAD_TAGS_SUCCESS,
    tags,
  };
}

/**
 * Dispatched when loading the tags fails
 *
 * @param  {boolean} error The error
 *
 * @return {object}       An action object with a type of LOAD_TAGS_FAILURE passing the error
 */
export function tagsLoadingError(error) {
  return {
    type: LOAD_TAGS_FAILURE,
    error,
  };
}

/**
 * Load the posts, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_POSTS
 */
export function loadPosts(session) {
  return {
    type: LOAD_POSTS,
    session,
  };
}

/**
 * Dispatched when the posts are loaded by the request saga
 *
 * @param  {array} posts The posts data
 *
 * @return {object}      An action object with a type of LOAD_POSTS_SUCCESS passing the posts
 */
export function postsLoaded(posts) {
  return {
    type: LOAD_POSTS_SUCCESS,
    posts,
  };
}

/**
 * Dispatched when loading the posts fails
 *
 * @param  {boolean} error The error
 *
 * @return {object}       An action object with a type of LOAD_POSTS_FAILURE passing the error
 */
export function postsLoadingError(error) {
  return {
    type: LOAD_POSTS_FAILURE,
    error,
  };
}
