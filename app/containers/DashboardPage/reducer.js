/*
 *
 * DashboardPage reducer
 *
 */

 import { fromJS } from 'immutable';
 import {
   LOAD_POSTS,
   LOAD_POSTS_SUCCESS,
   LOAD_POSTS_FAILURE,
   LOAD_TAGS,
   LOAD_TAGS_SUCCESS,
   LOAD_TAGS_FAILURE,
 } from './constants';

 const initialState = fromJS({
   posts: { ids: [] },
   tags: { ids: [] },
   loading: false,
   error: false,
 });

 function dashboardPageReducer(state = initialState, action) {
   switch (action.type) {
     case LOAD_POSTS:
     case LOAD_TAGS:
       return state
         .set('loading', true);
     case LOAD_POSTS_SUCCESS:
       return state
         .set('posts', action.posts)
         .set('loading', false)
         .set('error', false);
     case LOAD_TAGS_SUCCESS:
       return state
         .set('tags', action.tags)
         .set('loading', false)
         .set('error', false);
     case LOAD_POSTS_FAILURE:
     case LOAD_TAGS_FAILURE:
       return state
         .set('loading', false)
         .set('error', true);
     default:
       return state;
   }
 }

 export default dashboardPageReducer;
