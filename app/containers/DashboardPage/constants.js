/*
 *
 * DashboardPage constants
 *
 */

 export const LOAD_TAGS = 'blog/DashboardPage/LOAD_TAGS';
 export const LOAD_TAGS_SUCCESS = 'blog/DashboardPage/LOAD_TAGS_SUCCESS';
 export const LOAD_TAGS_FAILURE = 'blog/DashboardPage/LOAD_TAGS_FAILURE';
 export const LOAD_POSTS = 'blog/DashboardPage/LOAD_POSTS';
 export const LOAD_POSTS_SUCCESS = 'blog/DashboardPage/LOAD_POSTS_SUCCESS';
 export const LOAD_POSTS_FAILURE = 'blog/DashboardPage/LOAD_POSTS_FAILURE';
