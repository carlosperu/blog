import { createSelector } from 'reselect';

/**
 * The global state selectors
 */
const selectDashboard = () => (state) => state.get('dashboardPage');

/**
 * Default selector used by DashboardPage
 */

const selectPosts = () => createSelector(
  selectDashboard(),
  (dashboardState) => dashboardState.get('posts'),
);

const selectTags = () => createSelector(
  selectDashboard(),
  (dashboardState) => dashboardState.get('tags'),
);

const selectLoading = () => createSelector(
  selectDashboard(),
  (dashboardState) => dashboardState.get('loading'),
);

const selectError = () => createSelector(
  selectDashboard(),
  (dashboardState) => dashboardState.get('error'),
);

export {
  selectDashboard,
  selectPosts,
  selectTags,
  selectLoading,
  selectError,
};
