/*
 *
 * DashboardPage
 *
 */

import React, { PropTypes } from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import List from 'components/List';
import ListItem from 'components/ListItem';
import PostList from 'components/PostList';
import LoadingIndicator from 'components/LoadingIndicator';
import { loadTags, loadPosts } from './actions';
import { checkSession } from '../App/actions';
import { selectTags, selectLoading, selectError, selectPosts } from './selectors';
import { selectSession } from '../App/selectors';

export class DashboardPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    const { session } = this.props;
    this.props.checkSession(session);
    this.props.loadTags(session);
    this.props.loadPosts(session);
  }

  render() {
    let mainContent = null;
    let postContent = null;

    // Show a loading indicator when we're loading
    if (this.props.loading) {
      mainContent = (<LoadingIndicator />);

    // Show an error if there is one
    } else if (this.props.error !== false) {
      const ErrorComponent = () => (
        <ListItem item={'Something went wrong, please try again!'} />
      );
      mainContent = (<List component={ErrorComponent} />);

    // If we're not loading, don't have an error and there are posts, show the posts
    } else {
      if (this.props.tags !== false) {
        mainContent = !this.props.tags || !this.props.tags.ids ?
          (<div>No content</div>) :
          (<ul>
            <li>ID: TAG</li>
            {this.props.tags.ids.map((id) => <li key={id}>{id}: {this.props.tags[id].name}</li>)}
          </ul>);
      }
      if (this.props.posts !== false) {
        postContent = (<PostList posts={this.props.posts} />);
      }
    }

    return (
      <section>
        <Helmet
          title="DashboardPage"
          meta={[
            { name: 'description', content: 'Description of DashboardPage' },
          ]}
        />
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              {mainContent}
              {postContent}
            </div>
          </div>
        </div>
      </section>
    );
  }
}

DashboardPage.propTypes = {
  posts: PropTypes.object.isRequired,
  tags: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  session: PropTypes.object,
  loadPosts: PropTypes.func,
  loadTags: PropTypes.func,
  checkSession: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  posts: selectPosts(),
  tags: selectTags(),
  loading: selectLoading(),
  error: selectError(),
  session: selectSession(),
});

export function mapDispatchToProps(dispatch) {
  return {
    loadPosts: (session) => dispatch(loadPosts(session)),
    loadTags: (session) => dispatch(loadTags(session)),
    checkSession: (session) => dispatch(checkSession(session)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardPage);
