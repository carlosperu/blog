import axios from 'axios';

// TODO: Refactor this. Some of these functions are unnecessary.

export const API_HOST = 'http://api.polyglotcarlos.com';
/**
 * Send request to endpoint, returning a response
 *
 * @param  {string} endpoint       The endpoint we want to request
 *
 * @return {object}           The response data
 */
export function postRequestHeadersResponse(endpoint, params, headers) {
  return axios.post(endpoint, params, headers)
  .then((response) => response.headers)
  .catch((error) => error);
}

/**
 * Send request to endpoint, returning a response
 *
 * @param  {string} endpoint       The endpoint we want to request
 *
 * @return {object}           The response data
 */
export function postRequest(endpoint, params, headers) {
  return axios.post(endpoint, params, headers)
  .then((response) => response)
  .catch((error) => error);
}

/**
 * Send request to endpoint, returning a response
 *
 * @param  {string} endpoint       The endpoint we want to request
 *
 * @return {object}           The response data
 */
export function patchRequestHeadersResponse(endpoint, params, headers) {
  return axios.patch(endpoint, params, headers)
  .then((response) => response.headers)
  .catch((error) => error);
}

/**
 * Send request to endpoint, returning a response
 *
 * @param  {string} endpoint       The endpoint we want to request
 *
 * @return {object}           The response data
 */
export function patchRequest(endpoint, params, headers) {
  return axios.patch(endpoint, params, headers)
  .then((response) => response)
  .catch((error) => error);
}

/**
 * Send request to endpoint, returning a response
 *
 * @param  {string} endpoint       The endpoint we want to request
 *
 * @return {object}           The response data
 */
export function getRequestBodyResponse(endpoint) {
  return axios.get(endpoint)
  .then((response) => response.data)
  .catch((error) => error);
}

/**
 * Send request to endpoint, returning a response
 *
 * @param  {string} endpoint       The endpoint we want to request
 *
 * @return {object}           The response data
 */
export function getRequest(endpoint, headers) {
  return axios.get(endpoint, headers)
  .then((response) => response)
  .catch((error) => error);
}

/**
 * Send request to endpoint, returning a response
 *
 * @param  {string} endpoint       The endpoint we want to request
 */
export function deleteRequest(endpoint, headers) {
  return axios.delete(endpoint, headers)
  .then((response) => response)
  .catch((error) => error);
}
