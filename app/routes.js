// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';
import { UserAuthWrapper as userAuthWrapper } from 'redux-auth-wrapper';
import { selectSession, selectLoading } from './containers/App/selectors';
import { routerActions } from 'react-router-redux';
import LoadingPage from 'containers/LoadingPage';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const Auth = userAuthWrapper({ // eslint-disable-line new-cap
  // how to get the user state
  authSelector: selectSession(), // eslint-disable-line no-unused-vars
  authenticatingSelector: selectLoading(),
  LoadingComponent: LoadingPage,
  redirectAction: routerActions.replace, // the redux action to dispatch for redirect
  failureRedirectPath: '/caleta',
  wrapperDisplayName: 'UserAuth', // a nice name for this auth check
});

const NotAuth = userAuthWrapper({
  authSelector: selectSession(),
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserNotAuth',
  // Want to redirect the user when they are done loading and authenticated
  predicate: (session) => session === null,
  failureRedirectPath: (state, ownProps) => ownProps.location.query.redirect || '/caletaza',
  allowRedirectBack: false,
});

const Deauth = userAuthWrapper({
  authSelector: selectSession(),
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserDeauth',
  failureRedirectPath: '/caletaza',
  allowRedirectBack: false,
});

const loadModule = (cb, hoc = null) => (componentModule) => {
  if (hoc) cb(null, hoc(componentModule.default));
  else cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/',
      name: 'blogPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/BlogPage/reducer'),
          System.import('containers/BlogPage/sagas'),
          System.import('containers/BlogPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('blogPage', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/about',
      name: 'aboutPage',
      getComponent(nextState, cb) {
        System.import('containers/AboutPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: '/logout',
      name: 'logoutPage',
      getComponent(location, cb) {
        const importModules = Promise.all([
          System.import('containers/LogoutPage/sagas'),
          System.import('containers/LogoutPage'),
        ]);

        const renderRoute = loadModule(cb, Deauth);

        importModules.then(([sagas, component]) => {
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/caleta',
      name: 'loginPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/LoginPage/sagas'),
          System.import('containers/LoginPage'),
        ]);

        const renderRoute = loadModule(cb, NotAuth);

        importModules.then(([sagas, component]) => {
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/caletaza',
      name: 'dashboardPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/DashboardPage/reducer'),
          System.import('containers/DashboardPage/sagas'),
          System.import('containers/DashboardPage'),
        ]);

        const renderRoute = loadModule(cb, Auth);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('dashboardPage', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/tag/:permalink',
      name: 'tagPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/TagPage/reducer'),
          System.import('containers/TagPage/sagas'),
          System.import('containers/TagPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('tagPage', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/postEdit/:postId',
      name: 'postEdit',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/PostEdit/reducer'),
          System.import('containers/PostEdit/sagas'),
          System.import('containers/PostEdit'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('postEdit', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/postNew',
      name: 'postNew',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/PostNew/reducer'),
          System.import('containers/PostNew/sagas'),
          System.import('containers/PostNew'),
        ]);

        const renderRoute = loadModule(cb, Auth);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('postNew', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/post/:permalink',
      name: 'postPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('containers/PostPage/reducer'),
          System.import('containers/PostPage/sagas'),
          System.import('containers/PostPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('postPage', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        System.import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
