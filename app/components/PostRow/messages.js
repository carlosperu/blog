/*
 * PostRow Messages
 *
 * This contains all the text for the PostRow component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  continueLink: {
    id: 'blog.components.PostRow.continueLink',
    defaultMessage: 'Continue Reading...',
  },
  unpublished: {
    id: 'blog.components.PostRow.unpublished',
    defaultMessage: 'Not published yet',
  },
});
