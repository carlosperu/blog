/**
*
* PostRow
*
*/

import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import { FormattedMessage } from 'react-intl';
import messages from './messages';


function PostRow({ post }) {
  const postDateTime = post.publishedDate ? new Date(parseInt(post.publishedDate, 10)) : null;
  const dateContent = postDateTime ? (
    <p className="time">{postDateTime.toLocaleDateString()}<span>{'  '.replace(/ /g, '\u00a0')}</span>{postDateTime.toLocaleTimeString()}</p>
  ) : (<p className="time"><FormattedMessage {...messages.unpublished} /></p>);
  return (
    <div className="row">
      <div className="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
        <h3><Link to={`/post/${post.permalink}`}>{post.title}</Link></h3>
        {dateContent}
        <p dangerouslySetInnerHTML={{ __html: `${post.content.substr(0, 200)}...` }}></p>
        <p><Link to={`/post/${post.permalink}`}><FormattedMessage {...messages.continueLink} /></Link></p>
      </div>
    </div>
  );
}

PostRow.propTypes = {
  post: PropTypes.object.isRequired,
};

export default PostRow;
