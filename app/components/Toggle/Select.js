import styled from 'styled-components';

const Select = styled.select`
  height: 20px;
  color: white;
  background: #18BC9C;
  text-transform: uppercase;
  margin: 12px 20px;
`;

export default Select;
