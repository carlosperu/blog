/**
*
* Footer
*
*/

import React from 'react';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class Footer extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <footer className="text-center">
        <div className="footer-below">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <FormattedMessage {...messages.footer} />
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
