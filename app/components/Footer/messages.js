/*
 * Footer Messages
 *
 * This contains all the text for the Footer component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  footer: {
    id: 'blog.components.Footer.header',
    defaultMessage: 'Copyright © Polyglot Carlos 2017',
  },
});
