/**
*
* PostList
*
*/

import React, { PropTypes } from 'react';
import PostRow from '../PostRow';


function PostList({ posts }) {
  if (!posts || !posts.ids) return (<div>No content</div>);
  return (
    <div>{posts.ids.map((postId) => <PostRow key={postId} post={posts[postId]} />)}</div>
  );
}

PostList.propTypes = {
  posts: PropTypes.object,
};

export default PostList;
