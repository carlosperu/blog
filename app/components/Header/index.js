/**
*
* Header
*
*/

import React from 'react';
import HeaderMenu from 'containers/HeaderMenu';

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <nav id="mainNav" className="navbar navbar-default navbar-fixed-top navbar-custom">
        <HeaderMenu />
      </nav>
    );
  }
}

export default Header;
