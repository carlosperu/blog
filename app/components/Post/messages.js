/*
 * Post Messages
 *
 * This contains all the text for the Post component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  unpublished: {
    id: 'blog.components.PostRow.unpublished',
    defaultMessage: 'Not published yet',
  },
});
