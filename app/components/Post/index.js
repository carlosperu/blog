/**
*
* Post
*
*/

import React, { PropTypes } from 'react';

import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router';
import messages from './messages';
import TagLink from '../TagLink';


function Post({ edit, post, tags }) {
  // We create a date object from a string containing the time in milisecs.
  // If the post has not been published yet, the string will be empty.
  const postDateTime = post.publishedDate ? new Date(parseInt(post.publishedDate, 10)) : null;
  const dateContent = postDateTime ? (
    <p className="time">{postDateTime.toLocaleDateString()}<span>{'  '.replace(/ /g, '\u00a0')}</span>{postDateTime.toLocaleTimeString()}</p>
  ) : (<p className="time"><FormattedMessage {...messages.unpublished} /></p>);
  const editLink = edit ? (<Link to={`/postEdit/${post.id}`}>Edit</Link>) : null;
  return (
    <div className="row">
      <div className="col-lg-12 text-center">
        <h2>{post.title} {editLink}</h2>
        <hr className="star-primary" />
      </div>
      {/* toLocaleDateString gets date from Date object */}
      {/* toLocaleTimeString gets time from Date object */}
      {/* {'  '.replace(/ /g, '\u00a0')} adds a blank space in JSX */}
      <div className="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
        {dateContent}
        <p dangerouslySetInnerHTML={{ __html: post.content }}></p>
        {/* We add the tag links at the bottom of post */}
        <TagLink tags={tags} tagIds={post.tagIds} />
      </div>
    </div>
  );
}

Post.propTypes = {
  post: PropTypes.object.isRequired,
  tags: PropTypes.object.isRequired,
  edit: PropTypes.bool.isRequired,
};

export default Post;
