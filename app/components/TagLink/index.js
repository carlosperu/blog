/**
*
* TagLink
*
* This component provides tag links from an object containing tag objects
* and an array of tag id's that indicate the order of the tags.
*
*/

import React, { PropTypes } from 'react';
import { Link } from 'react-router';

function TagLink({ tags, tagIds }) {
  return (
    <p>
      {tagIds.map((tagId) => <span key={tagId}><Link to={`/tag/${tags[tagId].permalink}`}>{tags[tagId].name}</Link>{'   '.replace(/ /g, '\u00a0')}</span>)}
    </p>
  );
}

TagLink.propTypes = {
  tags: PropTypes.object.isRequired,
  tagIds: PropTypes.array.isRequired,
};

export default TagLink;
