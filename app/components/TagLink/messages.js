/*
 * TagLink Messages
 *
 * This contains all the text for the TagLink component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.TagLink.header',
    defaultMessage: 'This is the TagLink component !',
  },
});
