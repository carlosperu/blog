/**
*
* LoadingSign
*
*/

import React from 'react';

import { FormattedMessage } from 'react-intl';
import messages from './messages';


function LoadingSign() {
  return (
    <section>
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center">
            <FormattedMessage {...messages.loading} />
          </div>
        </div>
      </div>
    </section>
  );
}

export default LoadingSign;
