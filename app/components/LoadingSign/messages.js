/*
 * LoadingSign Messages
 *
 * This contains all the text for the LoadingSign component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  loading: {
    id: 'blog.components.LoadingSign.loading',
    defaultMessage: 'Logging in...',
  },
});
